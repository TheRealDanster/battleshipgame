import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * @author Daniel Tompkins
 */
public class Board {
	private BoardCell[][] grid = new BoardCell[10][10];
	private ArrayList<Ship> ships;
	private int shipsLeft = 0;

	public Board() {
		// Create ships array
		this.ships = new ArrayList<>();
    	this.ships.add(new Ship("Aircraft carrier", 5));
      	this.ships.add(new Ship("Battleship", 4));
      	this.ships.add(new Ship("Cruiser", 3));
      	this.ships.add(new Ship("Submarine", 3));
		this.ships.add(new Ship("Destroyer", 2));
	}

	/**
	 * Creates the game board
	 */
	public void create() {
		// Resets shipsLeft to the amount of ships
		this.shipsLeft = ships.size();

		boolean placeShip = true;
		ArrayList<String> direction = new ArrayList<>();
		direction.add("north");
		direction.add("south");
		direction.add("east");
		direction.add("west");

		initializeGrid();

		// Iterate over ships array
		for (Ship ship : ships) {
			ship.setHitCount(0);
			Collections.shuffle(direction);
			do {
				// Generate random coordinates
				Random random = new Random();
				int initialRow = random.nextInt(10);
				int initialCol = random.nextInt(10);
				placeShip = true;

				// Iterate over direction
				for (String dir : direction) {
					int row = initialRow;
					int col = initialCol;
					// Loop for length of ship
					for (int length = 0; length < ship.getLength(); length++) {
						// Check if cell is empty and check if coordinate are in grid bounds
						if (row >= 0 && row <= 9 && col >= 0 && col <= 9 && grid[row][col].getShip() == null) {
							// Increment/Decrement in appropriate direction
							switch (dir) {
								case "north":
									row--;
									break;
								case "south":
									row++;
									break;
								case "east":
									col++;
									break;
								case "west":
									col--;
									break;
							}
						} else {
							// Break out of ship length loop and get new direction
							placeShip = false;
							break;
						}
					}
					if (placeShip) {
						// Loop for length of ship
						for (int length = 0; length < ship.getLength(); length++) {
							// Add ship to cells
							grid[initialRow][initialCol].setShip(ship);
							switch (dir) {
								case "north":
									initialRow--;
									break;
								case "south":
									initialRow++;
									break;
								case "east":
									initialCol++;
									break;
								case "west":
									initialCol--;
									break;
							}
						}
						break;
					}
				}
			} while (!placeShip);
		}
		;
	}

	public void initializeGrid() {
		// Initialize grid with Cell objects
		for (int row = 0; row < 10; row++) {
			for (int col = 0; col < 10; col++) {
				grid[row][col] = new BoardCell();
			}
		}
	}

	public BoardCell getCell(int row, int col) {
		return this.grid[row][col];
	}

	public int getShipsLeft() {
		return shipsLeft;
	}

	public void shipSunk() {
		this.shipsLeft--;
	}

	@Override
	public String toString() {
		StringBuilder out = new StringBuilder("");
		String cellCode = "";
		for (int row = 0; row < 10; row++) {
			for (int col = 0; col < 10; col++) {
				if (grid[row][col].getShip() != null) {
					cellCode = String.valueOf(this.grid[row][col].getShip().getType().charAt(0));
				} else {
					cellCode = "_";
				}
				out.append(String.format("%s,", cellCode));
			}
		}
		return (out.toString()).substring(0, out.toString().length() - 1);
	}
}
