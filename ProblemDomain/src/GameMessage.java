import java.io.Serializable;

/**
 * @author Daniel Tompkins
 */
public class GameMessage implements Serializable {
	private String action = "";
	private String value = "";

	public GameMessage() {
	}

	public GameMessage(String action, String value) {
		this.action = action;
		this.value = value;
	}

	public String getAction() {
		return action;
	}

	public String getValue() {
		return value;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return String.format("%s: %s\n", this.action, this.value);
	}
}
