/**
 * @author Daniel Tompkins
 */
public class Ship {
	private String type = "";
	private int length = 0;
	private int hitCount = 0;

	public Ship(String type, int length) {
		this.type = type;
		this.length = length;
	}

	public String getType() {
		return type;
	}

	public int getLength() {
		return length;
	}

	public int getHitCount() {
		return hitCount;
	}

	public void setHitCount(int hitCount) {
		this.hitCount = hitCount;
	}

	/**
	 * Increments the hit count for a ship
	 */
	public void incrementHitCount() {
		this.hitCount++;
	}

	/**
	 * Checks if a ship has been sunk
	 * @return True if ship is sunk, false if not
	 */
	public boolean isSunk() {
		return (this.length == this.hitCount);
	}
}


