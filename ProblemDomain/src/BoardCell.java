/**
 * @author Daniel Tompkins
 */
public class BoardCell {
	private boolean hit = false;
	private boolean miss = false;
	private Ship ship = null;

	public BoardCell() {
	}

	public boolean isHit() {
		return hit;
	}

	public void setHit(boolean hit) {
		this.hit = hit;
		if (hit) {
			this.ship.incrementHitCount();
		}
	}

	public boolean isMiss() {
		return miss;
	}

	public void setMiss(boolean miss) {
		this.miss = miss;
	}

	public void setShip(Ship ship) {
		this.ship = ship;
	}

	public Ship getShip() {
		return ship;
	}
}
