import java.io.*;
import java.net.*;

/**
 * @author Daniel Tompkins
 */
public class MessageHandler implements Runnable {
	private Client client;
	private Socket server;
	private ObjectInputStream ois;

	public MessageHandler(Client client, Socket server, ObjectInputStream ois) {
		this.client = client;
		this.server = server;
		this.ois = ois;
	}

	@Override
	public void run() {
		while (!this.server.isClosed()) {
			try {
				Object object = this.ois.readObject();
				if (object instanceof GameMessage) {
					GameMessage gameMessage = (GameMessage) object;
					this.client.receiveMessage(gameMessage);
				}
			} catch (IOException e) {
				e.printStackTrace();
				break;
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		}
	}
