/**
 * This launcher *should* not be necessary but due to deployment issues with JavaFX
 * and executable JARs we are using it as a shim into the actual JavaFX launcher class.
 * @see <a href="https://github.com/openjfx/openjfx-docs/issues/90">Error:Java FX Packager: Can't build artifact - fx:deploy is not available in this JDK</a>
 */

/**
 * @author Daniel Tompkins
 */
public class ClientLauncher {
	public static void main(String[] args) {
		App.main(args);
	}
}
