import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Dialog;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

/**
 * @author Daniel Tompkins
 */
public class ConnectDialog extends Dialog {

	public ConnectDialogController.ConnectInfo display(Stage parentStage) {
		Stage window = new Stage();
		ConnectDialogController controller = null;
		try {
			window.initStyle(StageStyle.UNDECORATED);
			window.initModality(Modality.APPLICATION_MODAL);
			window.setWidth(550);
			window.setHeight(300);
			FXMLLoader loader = new FXMLLoader(getClass().getResource("connect-dialog.fxml"));
			Parent root = loader.load();
			controller = loader.getController();
			controller.initData(window);
			window.setScene(new Scene(root, 550, 300));
			window.setX(parentStage.getX() + parentStage.getWidth() / 2 - window.getWidth() / 2);
			window.setY(parentStage.getY() + parentStage.getHeight() / 2 - window.getHeight() / 2);
			window.showAndWait();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return controller.getConnectInfo();
	}

}