import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * @author Daniel Tompkins
 */
public class YesNoDialogController {

	@FXML private Label messageLabel;
	@FXML private Button yesButton;
	@FXML private Button noButton;

	private Stage stage = null;
	private boolean result = false;

	@FXML
	public void initData(Stage stage, String message, String yesButtonText, String noButtonText) {
		this.stage = stage;
		this.yesButton.setText(yesButtonText);
		this.noButton.setText(noButtonText);
		this.messageLabel.setText(message);
	}

	@FXML
	public void initialize() {

	}

	public boolean result() {
		return result;
	}

	@FXML
	private void handleYesButtonAction(ActionEvent event) {
		this.result = true;
		this.stage.close();
	}

	@FXML
	private void handleNoButtonAction(ActionEvent event) {
		this.result = false;
		this.stage.close();
	}

}
