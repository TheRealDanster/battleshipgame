import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Daniel Tompkins
 */
public class Controller {

	final int GRID_ROWS = 10;
	final int GRID_COLS = 10;

	private Stage stage = null;
	@FXML	private Text statusText;
	@FXML	private Label chatWithLabel;
	@FXML	private TextField chatTextField;
	@FXML	private TextArea chatTextArea;
	@FXML	private Button chatSendButton;
	@FXML	private Button fireButton;
	@FXML	private Button startButton;
	@FXML	private GridPane playerGrid;
	@FXML	private GridPane enemyGrid;

	private PlayerGridCell[][] playerGridLookup;
	private EnemyGridCell[][] enemyGridLookup;
	private GridCell targetGridCell = null;
	private TickerDisplay tickerDisplay = null;
	private String username = "";
	private String opponentName = "";

	private Client client;

	@FXML
	public void initialize() {
		 // JavaFX GridPane offers no easy way to get the contents of a cell at x,y.
		 // To work around this, I'm using a couple of 2D arrays to reference each cell in the grids and provide an easy way to lookup cell content.
		this.playerGridLookup = new PlayerGridCell[GRID_ROWS][GRID_COLS];
		this.enemyGridLookup = new EnemyGridCell[GRID_ROWS][GRID_COLS];
		initializeGrids();

		// Initialize the scrolling status message display
		statusText.setFont(Font.loadFont(Controller.class.getResourceAsStream("digital-7.ttf"), 40));
		statusText.setTextOrigin(VPos.TOP);
		statusText.setY(10.0);
		statusText.setFill(Color.web("#03e415"));
		this.tickerDisplay = new TickerDisplay(this.statusText);

		// No chatting until both players are connected
		chatSendButton.setDisable(true);

		client = new Client(this);

		this.fireButton.setVisible(false);
		this.fireButton.setManaged(false);
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	@FXML
	/**
	 * Open the connection dialog.
	 */
	private void handleStartButtonAction(ActionEvent event) {
		// Display dialog to get connection info and then connect to the server
		ConnectDialog connectDialog = new ConnectDialog();
		ConnectDialogController.ConnectInfo connectInfo = connectDialog.display(this.stage);
		if (connectInfo != null) {
			try {
				client.connect(connectInfo.getUserName(), connectInfo.getIp(), connectInfo.getPort());
				this.startButton.setVisible(false);
				this.startButton.setManaged(false);
				this.username = connectInfo.getUserName();
			} catch (IOException e) {
				this.tickerDisplay.addMessage("Unable to connect. Server unavailable.", 1, 5, true);
			}
		}
	}

	@FXML
	/**
	 * Send a 'fire' message when the fire button is clicked and they have selected a target
	 */
	private void handleFireButtonAction(ActionEvent event) {
		if (this.targetGridCell != null) {
			String row = String.valueOf(this.targetGridCell.row);
			String col = String.valueOf(this.targetGridCell.col);
			client.sendMessage("fire", row + col);
		} else {
			this.tickerDisplay.stopMessage();
			this.tickerDisplay.addMessage("Pick a target first...", 1, 5, true);
		}
	}

	@FXML
	/**
	 * Send a chat message to the other player
	 */
	private void handleChatSendButtonAction(ActionEvent event) {
		String message = this.chatTextField.getText();
		this.chatTextArea.setText(String.format("%s%s: %s\n", this.chatTextArea.getText(), username, message));
		client.sendMessage("chat", message);
		this.chatTextField.clear();
	}

	/**
	 * Called when the application is closing (because the window close button was clicked)
	 * @return true if the app should continue closing, false if not
	 */
	public boolean exitApplication() {
		YesNoDialog yesNoDialog = new YesNoDialog();
		return yesNoDialog.display(this.stage, "Quit Game\nAre you Sure?...", "Yes", "No");
	}

	/**
	 * Called when a game ends or a player disconnects
	 * @return true if the player wants to play again, false if not
	 */
	public void playAgain(Boolean winner) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				fireButton.setVisible(false);
				fireButton.setManaged(false);

				YesNoDialog yesNoDialog = new YesNoDialog();
				String message = String.format("%s\nPlay again?", (winner == null ? "Opponent quit." :(winner ? "You won!" : "You lost!")) );
				boolean result = yesNoDialog.display(stage, message, "Yes", "No");
				if (result) {
					for (int row = 0; row < GRID_ROWS; row++) {
						for (int col = 0; col < GRID_COLS; col++) {
							playerGridLookup[row][col].reset();
							enemyGridLookup[row][col].reset();
							tickerDisplay.clearQueue();
						}
					}
					client.sendMessage("playagain", "yes");
				} else {
					client.sendMessage("playagain", "no");
					Platform.exit();
				}
			}
		});
	}

	/**
	 * Send chat message if <ENTER> is pressed in the text field
	 * @param event
	 */
	@FXML
	public void onEnter(ActionEvent event) {
		this.chatSendButton.fire();
	}


	private void initializeGrids() {
		// Define grid row/col constraints for both grids
		for (int i = 0; i < GRID_ROWS; i++) {
			RowConstraints rowConstraints = new RowConstraints();
			rowConstraints.setVgrow(Priority.NEVER);
			rowConstraints.setMinHeight(30);
			rowConstraints.setMaxHeight(30);
			rowConstraints.setPrefHeight(30);
			this.playerGrid.getRowConstraints().add(rowConstraints);
			this.enemyGrid.getRowConstraints().add(rowConstraints);
		}
		for (int i = 0; i < GRID_COLS; i++) {
			ColumnConstraints colConstraints = new ColumnConstraints();
			colConstraints.setHgrow(Priority.NEVER);
			colConstraints.setMinWidth(30);
			colConstraints.setMaxWidth(30);
			colConstraints.setPrefWidth(30);
			this.playerGrid.getColumnConstraints().add(colConstraints);
			this.enemyGrid.getColumnConstraints().add(colConstraints);
		}

		// Add GridCell objects to each grid cell (note: player and enemy GridCells are slightly different)
		for (int row = 0; row < GRID_ROWS; row++) {
			for (int col = 0; col < GRID_COLS; col++) {
				// Player grid (mouse click handler is not needed if server generates the board)
				PlayerGridCell playerGridCell = new PlayerGridCell(row, col);
				this.playerGrid.add(playerGridCell, col, row);
				this.playerGridLookup[row][col] = playerGridCell;

				// Enemy grid
				EnemyGridCell enemyGridCell = new EnemyGridCell(this, row, col);
				this.enemyGrid.add(enemyGridCell, col, row);
				this.enemyGridLookup[row][col] = enemyGridCell;
			}
		}
	}

	/**
	 * Handles all incoming messages from the server
	 * @param message Message received from the server containing (action, text)
	 */
	public void receiveMessage(GameMessage message) {
		int row;
		int col;
		String[] data = null;
		switch (message.getAction().toLowerCase()) {
			case "searching":
				tickerDisplay.clearQueue();
				this.tickerDisplay.stopMessage();
				this.tickerDisplay.addMessage(message.getValue(), 0, 5);
				break;
			case "ready":
				tickerDisplay.clearQueue();
				client.sendMessage("name", username);
				this.tickerDisplay.stopMessage();
				this.tickerDisplay.addMessage(message.getValue(), 1, 5);
				chatSendButton.setDisable(false);
				break;
			case "board":
				findShips(message.getValue());
				this.tickerDisplay.stopMessage();
				client.sendMessage("ready", "");
				break;
			case "opponent":
				this.opponentName = message.getValue();
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						chatWithLabel.setText(String.format("Chatting with: %s", opponentName));
					}
				});
				break;
			case "wait":
				this.tickerDisplay.addMessage(message.getValue(), 0, 5);
				this.fireButton.setVisible(false);
				this.fireButton.setManaged(false);
				break;
			case "move":
				this.tickerDisplay.addMessage(message.getValue(), 0, 5);
				this.fireButton.setVisible(true);
				this.fireButton.setManaged(true);
				break;
			case "miss":
				data = message.getValue().split("\\|");
				this.tickerDisplay.stopMessage();
				this.tickerDisplay.addMessage(data[1], 1, 5);
				row = Character.getNumericValue(data[0].charAt(0));
				col = Character.getNumericValue(data[0].charAt(1));
				this.playerGridLookup[row][col].miss();
				break;
			case "hit":
				data = message.getValue().split("\\|");
				this.tickerDisplay.stopMessage();
				this.tickerDisplay.addMessage(data[1], 1, 5);
				row = Character.getNumericValue(data[0].charAt(0));
				col = Character.getNumericValue(data[0].charAt(1));
				this.playerGridLookup[row][col].hit();
				break;
			case "youmissed":
				data = message.getValue().split("\\|");
				this.tickerDisplay.stopMessage();
				this.tickerDisplay.addMessage(data[1], 1, 5);
				row = Character.getNumericValue(data[0].charAt(0));
				col = Character.getNumericValue(data[0].charAt(1));
				this.enemyGridLookup[row][col].miss();
				this.targetGridCell = null;
				break;
			case "youhit":
				data = message.getValue().split("\\|");
				this.tickerDisplay.stopMessage();
				this.tickerDisplay.addMessage(data[1], 1, 5);
				row = Character.getNumericValue(data[0].charAt(0));
				col = Character.getNumericValue(data[0].charAt(1));
				this.enemyGridLookup[row][col].hit();
				this.targetGridCell = null;
				break;
			case "sunk":
				this.tickerDisplay.addMessage(message.getValue(), 1, 5);
				break;
			case "gameover":
				data = message.getValue().split("\\|");
				this.tickerDisplay.stopMessage();
				this.tickerDisplay.addMessage(data[1], 0, 5);
				playAgain(data[0].equalsIgnoreCase("won"));
				break;
			case "playerlost":
				playAgain(null);
				break;
			case "chat":
				this.chatTextArea.setText(String.format("%s%s: %s\n", this.chatTextArea.getText(), this.opponentName, message.getValue()));
				break;
			default:
				// For debugging purposes; Prints if a message came through that has not been handled (has no case)
				App.DEBUG(String.format("[UNHANDLED MESSAGE] %s: %s", message.getAction(), message.getValue()));
				break;
		}
	}

	/**
	 * Rebuilds 2D array from server and figures out how the ships need to be placed on the GUI
	 * @param value String representation of the game board passed from the server
	 */
	public void findShips(String value) {
		String direction = "";
		String shipType = "";
		int shipLength = 0;
		ArrayList<String> ships = new ArrayList<>();

		// Rebuild grid array
		String[][] grid = new String[10][10];
		String[] data = value.split(",");
		int index = 0;
		for (int row = 0; row < GRID_ROWS; row++) {
			for (int col = 0; col < GRID_COLS; col++) {
				if (!data[index].equals("")) {
					grid[row][col] = data[index];
				}
				index++;
			}
		}
		index = 0;
		// Finds ships to be placed
		for (int row = 0; row < GRID_ROWS; row++) {
			for (int col = 0; col < GRID_COLS; col++) {
				if (!data[index].equals("_") && index < 99) {
					grid[row][col] = data[index];
					if (!data[index + 1].equals("_") && data[index + 1].equals(data[index])) {
						direction = "horizontal";
					} else {
						direction = "vertical";
					}
					switch (data[index]) {
						case "A":
							shipType = "carrier";
							shipLength = 5;
							break;
						case "B":
							shipType = "battleship";
							shipLength = 4;
							break;
						case "C":
							shipType = "cruiser";
							shipLength = 3;
							break;
						case "S":
							shipType = "submarine";
							shipLength = 3;
							break;
						case "D":
							shipType = "destroyer";
							shipLength = 2;
							break;
					}
					if (!ships.contains(shipType)) {
						ships.add(shipType);
						placeShips(direction, shipType, shipLength, row, col);
					}
				}
				index++;
			}
		}
	}

	/**
	 * Places ships on the GUI
	 * @param direction Direction the ship points (vertical, horizontal)
	 * @param shipType Type of ship being placed (Carrier, Battleship, Cruiser, Submarine, Destroyer)
	 * @param shipLength Length of the ship being placed (5-2)
	 * @param row Row the ship is being placed in (0-9)
	 * @param col Column the ship is being placed in (0-9)
	 */
	public void placeShips(String direction, String shipType, int shipLength, int row, int col) {
		int shipPart = 0;
		for (int length = 0; length < shipLength; length++) {
			shipPart++;
			switch (direction) {
				case "horizontal":
					this.playerGridLookup[row][col].setShip(String.format("/%s-h%s.png", shipType, shipPart));
					col++;
					break;
				case "vertical":
					this.playerGridLookup[row][col].setShip(String.format("/%s-v%s.png", shipType, shipPart));
					row++;
					break;
			}
		}
	}


	/**
	 * Represents a single cell in the 'enemy' grid pane
	 */
	class EnemyGridCell extends GridCell {
		private final Controller controller;
		private boolean attacked = false;

		public EnemyGridCell(Controller controller, int row, int col) {
			super(row, col);
			this.controller = controller;

			// Create a mouse click event handler for each cell
			setOnMouseClicked(e -> {
				if (!attacked) {
					// Clear any existing target
					if (controller.targetGridCell != null) {
						controller.targetGridCell.setStyle("-fx-background-image: none;");
					}

					// Add target icon at the location
					setStyle("-fx-background-image: url('/target.png'); -fx-background-position: center center;-fx-background-size: cover,auto;");

					// Save the target location
					controller.targetGridCell = this;
				}
			});
		}

		// Sets the hit graphic on the 'enemy' grid
		public void hit() {
			setStyle("-fx-background-image: url('/hit.png'); -fx-background-position: center center;-fx-background-size: cover,auto;");
			attacked = true;
		}

		// Sets the miss graphic on the 'enemy' grid
		public void miss() {
			setStyle("-fx-background-image: url('/miss.png'); -fx-background-position: center center;-fx-background-size: cover,auto;");
			attacked = true;
		}

		@Override
		// Removes all graphics from 'enemy' grid
		public void reset() {
			this.attacked = false;
			setStyle("-fx-background-image: none; -fx-background-position: center center;-fx-background-size: cover,auto;");
		}

	}

	/**
	 * Represents a single cell in the 'player' grid pane
	 */
	class PlayerGridCell extends GridCell {
		String shipImageURL = "";

		public PlayerGridCell(int row, int col) {
			super(row, col);
		}

		// Sets the ships graphic
		public void setShip(String shipImageURL) {
			this.shipImageURL = shipImageURL;
			setStyle("-fx-background-image: url('" + shipImageURL + "'); -fx-background-position: center center;-fx-background-size: cover,auto;");
		}

		// Sets the hit graphic on the 'player' grid
		public void hit() {
			setStyle("-fx-background-image: url('" + shipImageURL + "'), url('/hit.png'); -fx-background-position: center center;-fx-background-size: cover,auto;");
		}

		// Sets the miss graphic on the 'player' grid
		public void miss() {
			setStyle("-fx-background-image: url('/miss.png'); -fx-background-position: center center;-fx-background-size: cover,auto;");
		}

		@Override
		// Removes all graphics from 'player' grid
		public void reset() {
			this.shipImageURL = "";
			setStyle("-fx-background-image: none; -fx-background-position: center center;-fx-background-size: cover,auto;");
		}
	}

	/**
	 * Base class for a single cell in grid pane (abstract)
	 */
	abstract class GridCell extends Pane {
		private final int row;
		private final int col;

		public GridCell(int row, int col) {
			this.row = row;
			this.col = col;
		}

		abstract public void hit();

		abstract public void miss();

		abstract public void reset();
	}
}
