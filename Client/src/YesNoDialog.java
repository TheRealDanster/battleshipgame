import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Dialog;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

/**
 * @author Daniel Tompkins
 */
public class YesNoDialog extends Dialog {

	public boolean display(Stage parentStage, String message, String yesButtonText, String noButtonText) {
		Stage window = new Stage();
		YesNoDialogController controller = null;
		try {
			window.initStyle(StageStyle.UNDECORATED);
			window.initModality(Modality.APPLICATION_MODAL);
			window.setWidth(500);
			window.setHeight(200);
			FXMLLoader loader = new FXMLLoader(getClass().getResource("yes-no-dialog.fxml"));
			Parent root = loader.load();
			controller = loader.getController();
			controller.initData(window, message, yesButtonText, noButtonText);
			window.setScene(new Scene(root, 500, 200));
			window.setX(parentStage.getX() + parentStage.getWidth() / 2 - window.getWidth() / 2);
			window.setY(parentStage.getY() + parentStage.getHeight() / 2 - window.getHeight() / 2);
			window.showAndWait();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return controller.result();
	}
}
