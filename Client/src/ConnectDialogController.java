import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * @author Daniel Tompkins
 */
public class ConnectDialogController {

	@FXML private TextField usernameTextField;
	@FXML private TextField serverIPTextField;
	@FXML private TextField serverPortTextField;

	private ConnectInfo connectInfo = null;
	private Stage stage = null;

	@FXML
	public void initData(Stage stage) {
		this.stage = stage;
	}

	@FXML
	public void initialize() {

	}
	public ConnectInfo getConnectInfo() {
		return this.connectInfo;
	}

	@FXML
	private void handleOkButtonAction(ActionEvent event) {
		this.connectInfo = new ConnectInfo(this.usernameTextField.getText(), this.serverIPTextField.getText(), Integer.parseInt(this.serverPortTextField.getText()));
		this.stage.close();
	}

	@FXML
	private void handleCancelButtonAction(ActionEvent event) {
		this.connectInfo = null;
		this.stage.close();
	}

	public class ConnectInfo {
		private String userName = "";
		private String ip = "";
		private int port;

		public ConnectInfo(String userName, String ip, int port) {
			this.userName = userName;
			this.ip = ip;
			this.port = port;
		}

		public String getUserName() {
			return userName;
		}

		public String getIp() {
			return ip;
		}

		public int getPort() {
			return port;
		}
	}
}
