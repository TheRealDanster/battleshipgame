import java.io.*;
import java.net.Socket;

/**
 * @author Daniel Tompkins
 */
public class Client {

	private Socket socket;
	private ObjectOutputStream objectOutputStream;
	private ObjectInputStream objectInputStream;
	private Controller controller;

	public Client(Controller controller) {
		this.controller = controller;
	}

	/**
	 * Sends messages to the server and logs the message in client debug window
	 * @param action What the message is doing
	 * @param text What the message says
	 */
	public void sendMessage(String action, String text) {
		GameMessage message = new GameMessage(action, text);
		App.DEBUG(String.format("[SND MSG] <%s> %s", message.getAction(), message.getValue()));

		try {
			this.objectOutputStream.writeObject(message);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Unable to send message.");
		}
	}

	/**
	 * Connects the user to the server on the specified ip and port
	 * @param username Username of the client connecting
	 * @param ip IP the client is connecting to
	 * @param port Port the client is connecting to
	 * @throws IOException
	 */
	public void connect(String username, String ip, int port) throws IOException {
		socket = new Socket(ip, port);

		OutputStream outputStream = socket.getOutputStream();
		objectOutputStream = new ObjectOutputStream(outputStream);

		InputStream inputStream = socket.getInputStream();
		objectInputStream = new ObjectInputStream(inputStream);

		MessageHandler messageHandler = new MessageHandler(this, socket, objectInputStream);
		Thread thread = new Thread(messageHandler);
		thread.start();
	}

	/**
	 * Receives message from server logs it in client debug window and passes it to the controller
	 * @param message Message received from the server (action, text)
	 */
	public void receiveMessage(GameMessage message) {
		App.DEBUG(String.format("[RCV MSG] <%s> %s", message.getAction(), message.getValue()));
		controller.receiveMessage(message);
	}
}
