import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Daniel Tompkins
 */
public class TickerDisplay {

	private Text text = null;
	private Timeline timeline = null;
	private Queue<Message> messages = null;
	private Message currentMessage = null;

	public TickerDisplay(Text text) {
		this.text = text;
		this.timeline = new Timeline();
		this.messages = new LinkedList<>();
	}

	public void addMessage(String text, int cycles, int duration) {
		addMessage(text, cycles, duration, false);
	}

	public void addMessage(String text, int cycles, int duration, boolean error) {
		addMessage(text, cycles, duration, error, null);
	}

	public void addMessage(String text, int cycles, int duration, boolean error, Runnable action) {
		// Example usage for Runnable action:
		//	Runnable action = () -> {this.fireButton.setVisible(true); this.fireButton.setManaged(true);};
		//	this.tickerDisplay.addMessage(message.getValue(), 1, 5, false, action);

		Message message = new Message(text, cycles, duration, error, action);
		if (timeline.getStatus() != Animation.Status.RUNNING) {
			App.DEBUG("[TICKER DISPLAY]: add message (display)");
			displayMessage(message);
		}
		else {
			App.DEBUG("[TICKER DISPLAY]: add message (queue)");
			messages.add(message);
		}
	}

	/**
	 * Stops message currently being displayed
	 */
	public void stopMessage() {
		App.DEBUG("[TICKER DISPLAY]: stopped");
		this.text.setText("");
		this.timeline.stop();
		if (this.currentMessage != null && this.currentMessage.action != null) {
			new Thread(this.currentMessage.action).start();
		}
	}

	/**
	 * Clears queue of messages
	 */
	public void clearQueue() {
		this.messages.clear();
	}

	/**
	 * Displays message
	 * @param message Message ot be displayed
	 */
	private void displayMessage(Message message) {
		App.DEBUG("[TICKER DISPLAY]: " + message.text);

		this.currentMessage = message;
		this.text.setText(message.text.toUpperCase());

		if (message.error) {
			this.text.setFill(Color.web("#f23030"));
		} else {
			this.text.setFill(Color.web("#03e415"));
		}

		double sceneWidth = 600;
		double msgWidth = this.text.getLayoutBounds().getWidth();

		KeyValue initKeyValue = new KeyValue(this.text.translateXProperty(), sceneWidth);
		KeyFrame initFrame = new KeyFrame(Duration.ZERO, initKeyValue);

		KeyValue endKeyValue = new KeyValue(this.text.translateXProperty(), -1.0 * msgWidth);
		KeyFrame endFrame = new KeyFrame(Duration.seconds(message.duration), endKeyValue);

		this.timeline = new Timeline(initFrame, endFrame);
		this.timeline.setCycleCount( (message.cycles == 0)? Timeline.INDEFINITE : message.cycles);
		this.timeline.setOnFinished(this.onFinished);
		this.timeline.play();
	}

	/**
	 * Gets next meesage from queue if there is one
	 */
	EventHandler onFinished = new EventHandler<ActionEvent>() {
		public void handle(ActionEvent t) {
			App.DEBUG("[TICKER DISPLAY]: onFinished");
			if (currentMessage != null && currentMessage.action != null) {
				new Thread(currentMessage.action).start();
			}
			if (!messages.isEmpty()) {
				App.DEBUG(String.format("[TICKER DISPLAY]: Load message from queue"));
				displayMessage(messages.remove());
			}
		}
	};

	private class Message {
		private String text = "";
		private int cycles = 1;
		private int duration = 8;
		private boolean error = false;
		private Runnable action = null;

		public Message(String text, int cycles, int duration, boolean error, Runnable action) {
			this.text = text;
			this.cycles = cycles;
			this.duration = duration;
			this.error = error;
			this.action = action;
		}
	}
}
