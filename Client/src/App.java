import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Daniel Tompkins
 */
public class App extends Application {

	private static TextArea debugText;
	private static int debugID = 0;
	private static boolean debugMode = false;

	@Override
	public void start(Stage primaryStage) throws Exception {

		debugMode = getParameters().getRaw().contains("debug");
		debugID = System.identityHashCode(this);
		debugText = new TextArea();

		String titleText = String.format("Battleship Client%s", (debugMode) ? String.format(" [%d]", debugID) : "");
		App.DEBUG(String.format("%s running", titleText));
		FXMLLoader loader = new FXMLLoader(getClass().getResource("battleship.fxml"));
		Parent root = loader.load();
		primaryStage.setTitle(titleText);
		primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("icon.png")));
		Controller controller = loader.getController();
		controller.setStage(primaryStage);
		Scene scene = new Scene(root, 600, 850);
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);

		/**
		 * Intercept app close event and get confirmation that player wants to quit
		 */
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				if (controller.exitApplication()) {
					Platform.exit();
					System.exit(0);
				} else {
					event.consume();
				}
			}
		});

		/**
		 * Display a debug window on CTRL+ALT+D key combination in client
		 * Use: App.DEBUG("some debug info"); to output debugging info
		 */
		scene.setOnKeyPressed(e -> {
			if (e.isControlDown() && e.isAltDown() && e.getCode() == KeyCode.D) {
				showDebugWindow();
			}
		});
		primaryStage.show();

		if (debugMode) {
			showDebugWindow();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * Creates the debug window
	 */
	private static void showDebugWindow() {
		Stage debugStage = new Stage();
		BorderPane border = new BorderPane();
		Scene debugScene = new Scene(border, 600, 400);
		debugText.setFont(Font.loadFont(App.class.getResourceAsStream("RobotoMono.ttf"), 13));
		debugText.setEditable(false);
		debugText.setStyle("-fx-background-color: DARKGRAY;-fx-control-inner-background: #000000;-fx-text-fill: WHITE;");
		border.setCenter(debugText);
		debugStage.setTitle(String.format("Battleship Debug [%d]", debugID));
		debugStage.setScene(debugScene);
		debugStage.sizeToScene();
		debugStage.show();
	}

	/**
	 * Prints the debug messages to the debug window in the correct format
	 * @param message Message to be displayed in debug message
	 */
	public static void DEBUG(String message) {
		Platform.runLater(() -> {
			String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss.SSS"));
			debugText.appendText(String.format("%s %s\n", time, message));
		});
	}
}
