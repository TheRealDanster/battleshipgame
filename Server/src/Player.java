import javafx.application.Platform;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * @author Daniel Tompkins
 */
public class Player implements Runnable {
	private Socket socket;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private int id;
	private Game game = null;
	private Player opponent = null;
	private Board board = null;
	private String name = "";
	private boolean firstMove = false;
	private Boolean playAgain = null;

	public Player(int id, Socket socket, ObjectInputStream ois, ObjectOutputStream oos) {
		this.id = id;
		this.socket = socket;
		this.ois = ois;
		this.oos = oos;
		this.board = new Board();

		sendGameMessage(new GameMessage("searching", "Looking for opponent..."));
	}

	public Board getBoard() {
		return board;
	}

	public boolean isFirstMove() {
		return firstMove;
	}

	public void setFirstMove(boolean firstMove) {
		this.firstMove = firstMove;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Player getOpponent() {
		return opponent;
	}

	public void setOpponent(Player opponent) {
		this.opponent = opponent;
	}

	public Socket getSocket() {
		return socket;
	}

	public ObjectInputStream getOis() {
		return ois;
	}

	public ObjectOutputStream getOos() {
		return oos;
	}

	public void setPlayAgain(Boolean playAgain) {
		this.playAgain = playAgain;
	}

	public Boolean playAgain() {
		return playAgain;
	}

	/**
	 * Sends a game message
	 * @param message Message to be sent
	 */
	public void sendGameMessage(GameMessage message) {
		try {
			this.getOos().writeObject(message);

			Platform.runLater(() -> {
				BattleshipServer.DEBUG(String.format("[%s][SND MSG] <%s> %s", (getName().equals("") ? String.valueOf(this.id) : this.getName()), message.getAction(), message.getValue()));
			});

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Handles received game messages
	 * @param message Message to be handled
	 */
	public void receiveGameMessage(GameMessage message) {
		switch (message.getAction().toLowerCase()) {
			case "chat":
				opponent.sendGameMessage(message);
				break;
			case "name":
				this.name = message.getValue();
				BattleshipServer.DEBUG(String.format("[SVR] Client %s is now called '%s'", this.id, this.name));
				this.opponent.sendGameMessage(new GameMessage("opponent", this.name));
				sendGameMessage(new GameMessage("board", getBoard().toString()));
				break;
			case "ready":
				// Wait for both players to be ready
				do {
					try {
						Thread.sleep(250);
					} catch (InterruptedException e) {
					}
				} while (opponent.getName().equals(""));

				if (this.firstMove) {
					sendGameMessage(new GameMessage("move", "Your turn"));
				} else {
					sendGameMessage(new GameMessage("wait", String.format("Waiting for %s to attack", opponent.getName())));
				}
				break;
			case "fire":
				int row = Character.getNumericValue(message.getValue().charAt(0));
				int col = Character.getNumericValue(message.getValue().charAt(1));
				game.move(this, this.opponent, row, col);
				break;
			case "playagain":
				if (message.getValue().equalsIgnoreCase("yes")) {
					this.playAgain = true;
					this.sendGameMessage(new GameMessage("searching", "Searching for a new game"));
					if (opponent.playAgain != null && opponent.playAgain()) {
						game.newGame();
					} else if (opponent.playAgain != null && !opponent.playAgain()) {
						this.game.getBattleshipServer().endGame(this.game);
					}
				} else {
					this.playAgain = false;
					this.sendGameMessage(new GameMessage("disconnected", ""));
					try {
						BattleshipServer.DEBUG(String.format("[SVR] %s has left game %d.", this.getName(), this.game.getId()));
						this.ois.close();
						this.oos.close();
						this.socket.close();
					} catch (IOException e) {
					}
					if (opponent.playAgain != null) {
						this.game.getBattleshipServer().endGame(this.game);
						this.sendGameMessage(new GameMessage("newgame", "Searching for a new game"));
					}
				}
				break;
			case "disconnect":
				this.playAgain = false;
				this.opponent.sendGameMessage(new GameMessage("playerlost", (this.getName() + " has left the game.")));
				BattleshipServer.DEBUG(String.format("[SVR] %s has left game %d.", this.getName(), this.game.getId()));
				break;
		}
	}

	@Override
	public void run() {
		Thread thread = new Thread(new MessageHandler(this));
		thread.start();

		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
