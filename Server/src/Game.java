import java.util.Random;

/**
 * @author Daniel Tompkins
 */
public class Game {
	private int id;
	private Player player1;
	private Player player2;
	private BattleshipServer battleshipServer;

	public Game(int id, Player player1, Player player2, BattleshipServer battleshipServer) {
		this.id = id;
		this.player1 = player1;
		this.player1.setGame(this);
		this.player1.setOpponent(player2);

		this.player2 = player2;
		this.player2.setGame(this);
		this.player2.setOpponent(player1);

		this.battleshipServer = battleshipServer;

		// Randomly pick a player to go first
		Player player = new Random().nextBoolean() ? player1 : player2;
		player.getOpponent().setFirstMove(false);
		player.setFirstMove(true);

		newGame();
	}

	/**
	 * Starts a new game if both players said yes
	 */
	public void newGame() {
		BattleshipServer.DEBUG(String.format("[SVR] Starting new game [ID=%d]", this.id));
		player1.setPlayAgain(null);
		player2.setPlayAgain(null);

		this.player1.getBoard().create();
		this.player2.getBoard().create();

		player1.sendGameMessage(new GameMessage("ready", "Loading game..."));
		player2.sendGameMessage(new GameMessage("ready", "Loading game..."));
	}

	/**
	 * Checks if the player that fired hit an enemy vessel, sunk the vessel, and if they won the game
	 * @param player Player making the move
	 * @param opponent Opponent to be attacked
	 * @param row Row coordinate
	 * @param col Column coordinate
	 */
	public void move(Player player, Player opponent, int row, int col) {
		GameMessage playerMessage = new GameMessage();
		GameMessage opponentMessage = new GameMessage();

		BoardCell cell = opponent.getBoard().getCell(row, col);
		if (cell.getShip() != null) {
			// Hit!
			cell.setHit(true);

			// Check if final ship was sunk
			if (cell.getShip().isSunk() && opponent.getBoard().getShipsLeft() == 1) {
				opponent.getBoard().shipSunk();
				// Player that won gets to go first next time (if both players agree to play again)
				player.setFirstMove(true);
				opponent.setFirstMove(false);

				playerMessage.setAction("gameover");
				playerMessage.setValue(String.format("won|You won! You sunk the enemy %s.", cell.getShip().getType()));
				opponentMessage.setAction("gameover");
				opponentMessage.setValue(String.format("lost|You lost! Your %s was sunk.", cell.getShip().getType()));
				// If they didn't win did they sink a ship
			} else if (cell.getShip().isSunk()) {
				opponent.getBoard().shipSunk();
				playerMessage.setAction("sunk");
				playerMessage.setValue(String.format("%s%s|You sunk the enemy %s.", row, col, cell.getShip().getType()));
				opponentMessage.setAction("sunk");
				opponentMessage.setValue(String.format("%s%s|Your %s was sunk.", row, col, cell.getShip().getType()));
				// If they didn't sink a ship did they hit a ship
			} else {
				playerMessage.setAction("youhit");
				playerMessage.setValue(String.format("%s%s|You hit an enemy ship.", row, col));
				opponentMessage.setAction("hit");
				opponentMessage.setValue(String.format("%s%s|The enemy hit your %s.", row, col, cell.getShip().getType()));
			}
		} else {
			// Miss!
			playerMessage.setAction("youmissed");
			playerMessage.setValue(String.format("%s%s|Your shot missed the enemy ships.", row, col));
			opponentMessage.setAction("miss");
			opponentMessage.setValue(String.format("%s%s|The enemy shot missed your ships.", row, col));
		}
		player.sendGameMessage(playerMessage);
		opponent.sendGameMessage(opponentMessage);

		// Checks number of ships left on opponents board
		if (opponent.getBoard().getShipsLeft() != 0) {
			opponent.sendGameMessage(new GameMessage("move", "It's your move."));
			player.sendGameMessage(new GameMessage("wait", String.format("Waiting for %s to attack.", opponent.getName())));
		}
	}

	public BattleshipServer getBattleshipServer() {
		return battleshipServer;
	}

	public Player getPlayer1() {
		return player1;
	}

	public Player getPlayer2() {
		return player2;
	}

	public int getId() {
		return id;
	}
}
