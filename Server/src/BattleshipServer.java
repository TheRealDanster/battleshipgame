import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * @author Daniel Tompkins
 */
public class BattleshipServer extends Application {
	static private TextArea debugTextArea;

	private int gameNum = 0;
	private int playerNum = 0;

	ArrayList<Player> playerQueue = new ArrayList<>();

	@Override // Override the start method in the Application class
	public void start(Stage primaryStage) {
		BorderPane border = new BorderPane();
		Scene scene = new Scene(border, 600, 400);
		debugTextArea = new TextArea();
		debugTextArea.setFont(Font.loadFont(BattleshipServer.class.getResourceAsStream("RobotoMono.ttf"), 13));
		debugTextArea.setStyle("-fx-background-color: DARKGRAY;-fx-control-inner-background: #000000;-fx-text-fill: WHITE;");
		border.setCenter(debugTextArea);
		primaryStage.setTitle("Battleship Server");
		primaryStage.setScene(scene);
		primaryStage.sizeToScene();
		primaryStage.show();

		new Thread(() -> {
			DEBUG("Battleship Server running");
			try {
				// Create a server socket
				ServerSocket serverSocket = new ServerSocket(8000);
				DEBUG("[SVR] Waiting for connections...");

				while (true) {
					// Listen for a new connection request
					Socket socket = serverSocket.accept();
					playerNum++;

					InetAddress inetAddress = socket.getInetAddress();
					DEBUG(String.format("[SVR] Starting thread for client %d: %s %s", playerNum, inetAddress.getHostName(), inetAddress.getHostAddress()));

					InputStream inputStream = socket.getInputStream();
					ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);

					OutputStream outputStream = socket.getOutputStream();
					ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

					Player player = new Player(playerNum, socket, objectInputStream, objectOutputStream);
					new Thread(player).start();

					playerQueue.add(player);

					matchPlayers();
				}

			} catch (IOException ex) {
				System.err.println(ex);
			}
		}).start();
	}

	/**
	 * Match two players together
	 */
	public synchronized void matchPlayers() {
		// Create and start a new thread for the connection
		if (playerQueue.size() > 0 && playerQueue.size() % 2 == 0) {
			Player player1 = playerQueue.get(0);
			Player player2 = playerQueue.get(1);

			// Start a new game
			this.gameNum++;
			Game game = new Game(this.gameNum, player1, player2, this);

			// Remove players from matchmaking list
			playerQueue.remove(player1);
			playerQueue.remove(player2);
		}
	}

	/**
	 * Ends a game
	 * @param game Game to be ended
	 */
	public void endGame(Game game) {
		if (game.getPlayer1().playAgain()) {
			this.playerQueue.add(game.getPlayer1());
		}

		if (game.getPlayer2().playAgain()) {
			this.playerQueue.add(game.getPlayer2());
		}
		DEBUG(String.format("[SVR] Ending game [ID=%d]", game.getId()));
		game = null;
		matchPlayers();
	}

	public static void main(String[] args) {
		launch(args);
	}

	public static void DEBUG(String message) {
		Platform.runLater(() -> {
			String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss.SSS"));
			debugTextArea.appendText(String.format("%s %s\n", time, message));
		});
	}
}

