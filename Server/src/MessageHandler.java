import java.io.IOException;

/**
 * @author Daniel Tompkins
 */
public class MessageHandler implements Runnable {
	private Player player;

	public MessageHandler(Player player) {
		this.player = player;
	}

	@Override
	public void run() {
		while (!this.player.getSocket().isClosed()) {
			try {
				Object object = this.player.getOis().readObject();
				if (object instanceof GameMessage) {
					GameMessage gameMessage = (GameMessage) object;
					player.receiveGameMessage(gameMessage);
					BattleshipServer.DEBUG(String.format("[%s][RCV MSG] <%s> %s", this.player.getName(), gameMessage.getAction(), gameMessage.getValue()));
				}
			} catch (IOException e) {
				this.player.receiveGameMessage(new GameMessage("disconnect", ""));
				break;
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

}
